package main;

func main() {
    var a auto = 1;

    echo typeof a;

    a = "1";

    echo typeof a;

    echo typeof 1.2;

    echo typeof typeof a;

    echo typeof a == 1;

    echo typeof(a == 1);
}